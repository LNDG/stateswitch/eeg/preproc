%% Import ASC data export MAT data

% 170913 | JQK adapted function from MD's scripts
% 171221 | JQK adapted for study STSWD task
% 180205 | added older adults

% Convert relevant edf data to *.asc first! The tool can be found in the
% /C_data/B_eye/Z_Tools/ folder.

%% initialize

clear; close all; pack; clc;

%% load in path & toolboxes

pn.eeg_root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/A_preproc/SA_preproc_study/';
pn.eye_root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/SA_EEG/';
pn.eye_asc  = [pn.eye_root, 'A_preprocessing/B_data/B_eye_ascii/'];
pn.eye_mat  = [pn.eye_root, 'A_preprocessing/B_data/C_eye_mat/'];
pn.eye_logs = [pn.eye_root, 'A_preprocessing/Y_logs/asc2mat/']; mkdir(pn.eye_logs);
pn.eeglab   = [pn.eeg_root, 'T_Tools/eeglab14_1_1b/']; addpath(pn.eeglab);

%% open eeglab

eeglab % use eegh to get history of EEGlab commands!

fprintf('\n Reminder: Use Segmentation script to convert *.asc data to ft data format instead! \n')

%% datadef

condEEG = 'dynamic'; % Note: ET data were only recorded during the task.

%% define IDs for preprocessing

% IDs = dir('/Volumes/LNDG/Projects/StateSwitch/dynamic/raw/C_study/eeg/2*');
% IDs = {IDs(:).name}';
% IDs = cellfun(@(x){x(1:4)}, IDs);

% N = 48 YAs;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';'1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1213';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

% N = 53 OAs;
IDs = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

%% convert *asc ET data to *.mat (by subject and run)

unavailableData = NaN(numel(IDs), 4); % matrix for data by subject and run

for i = 1:length(IDs)
    % Define ID
    ID = IDs{i};
    % create log
    diary([pn.eye_logs, num2str(ID) ,'_', condEEG , '_eyeEEGnotes.txt']);
    for run = 1:4
        % output some info
        fprintf(['Processing ID ',num2str(ID), ', Run ',num2str(run),' on ', date, '\n']);
        fprintf(['Function is: ', mfilename('fullpath'), '\n']);
        try
            eyeFileIn   = [pn.eye_asc, 'S', num2str(ID), 'r', num2str(run), '.asc'];
            eyeFileOut  = [pn.eye_mat, 'S', num2str(ID), 'r', num2str(run), '.mat'];
            ET = parseeyelink(eyeFileIn,eyeFileOut);
            unavailableData(i, run) = 0;
        catch
            unavailableData(i, run) = ID;
            continue;
        end
    end % run loop
    diary off
end % subject loop