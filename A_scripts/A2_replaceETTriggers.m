% insert the ET triggers for the recordings as 80% of the recordings did
% not receve the triggers properly

% The strategy is to match the sample points at which a message has been
% sent via PTB to the ET and a trigger has simultaneously been sent.
% Obviously, we lose/have lost the ability to match the two datasets based
% on any jitter between the recordings, but the pilot looked stable enough
% for us not to care right now. Not that we would have any other choice
% anyways.

% 180103 | written by JQK for STSWD Study
% 180205 | included OA IDs, changed file I/O

%% copy eye tracker mat files

pn.originalETmats   = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/SA_EEG/A_preprocessing/B_data/C_eye_mat/';
pn.editedETmats     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/SA_EEG/A_preprocessing/B_data/D_eye_mat_edited/';

%% recreate trigger structure by using the eye tracker messages

% Subj%s Time%d %Run%d Block%d Trial%d Start    | 1, 2 (following 5 ms later)
% Run %d Ready screen onset                     | post 2 (button press)
% ... BlockCueOnset                             | 16
% ... FixCueOnset                               | 17
% ... PostCueFixOnset                           | 72
% ... StimOnset                                 | 20
% ... RDMUpdate                                 | 96
% ... RespOnset                                 | 24
% ... ResponseAcc                               | 130
% ... Trial (supposed to be called ITIOnset)    | 64
% ... Feedback                                  | before 128 (saving in between)
% terminate                                     | before 6, 8

triggerMatching{1,1} = {'Subj'};
triggerMatching{1,2} = 1;
triggerMatching{2,1} = {'BlockCueOnset'};
triggerMatching{2,2} = 16;
triggerMatching{3,1} = {'FixCueOnset'};
triggerMatching{3,2} = 17;
triggerMatching{4,1} = {'PostCueFixOnset'};
triggerMatching{4,2} = 72;
triggerMatching{5,1} = {'StimOnset'};
triggerMatching{5,2} = 20;
triggerMatching{6,1} = {'RDMUpdate'};
triggerMatching{6,2} = 96;
triggerMatching{7,1} = {'RespOnset'};
triggerMatching{7,2} = 24;
triggerMatching{8,1} = {'ResponseAcc'};
triggerMatching{8,2} = 130;
triggerMatching{9,1} = {'ITIOnset'};
triggerMatching{9,2} = 64;

% Note that for the concurrent extraction, the first and last event are
% extremely important. The last event will be the last occurence if
% multiple instances occur. Here, the final ITIOnset is optimal.

folderNames = dir([pn.originalETmats, 'S2*']);
folderNames = {folderNames(:).name};

% Triggers = zeros(188, 1682626);

for indFile = 1:numel(folderNames)
    
    disp(num2str(indFile));
    
    % load original ET data
    fileName = [pn.originalETmats, folderNames{indFile}];
    fileNameOut = [pn.editedETmats, folderNames{indFile}];
    originalET = load(fileName);
    
    % delete original (wrong) triggers
    
    originalET.data(:,9) = 0;
    
    % get trigger locations
    
    for indTrigger = 1:size(triggerMatching,1)-1
        findMat{indTrigger} = strfind({originalET.othermessages.msg}, triggerMatching{indTrigger,1});
        idx{indTrigger} = find(~cellfun(@isempty,findMat{indTrigger}));
        triggerMatchingVals{indTrigger,1} = [originalET.othermessages(idx{indTrigger}).timestamp]';
    end
    % for the final trigger, we need to use a trick:
    % select all the trials that do not have a trial number indicated
    for indTrigger = size(triggerMatching,1)        
        findNoTrial{1} = strfind({originalET.othermessages.msg}, 'Trial ');
        idxNoTrial{1} = find(~cellfun(@isempty,findNoTrial{1}));
        idxTrials = [];
        for indTrial = 1:8
            findTrials{indTrial} = strfind({originalET.othermessages.msg}, ['Trial ', num2str(indTrial)]);
            idxTrials = cat(2,idxTrials,find(~cellfun(@isempty,findTrials{indTrial})));
        end
        idxTrials = unique(idxTrials);
        idx{indTrigger} = setdiff(idxNoTrial{1}, idxTrials);
        triggerMatchingVals{indTrigger,1} = [originalET.othermessages(idx{indTrigger}).timestamp]';
    end
    
    triggerMatchingVals
    
    % enter trigger information
    
    originalET.event = [];
    for indTrigger = 1:size(triggerMatching,1)
        c = ismember(originalET.data(:,1), triggerMatchingVals{indTrigger,1});
        originalET.data(c,9) = triggerMatching{indTrigger,2};
        originalET.event = cat(1,originalET.event, ...
            [triggerMatchingVals{indTrigger,1}, ...
            repmat(triggerMatching{indTrigger,2},numel(triggerMatchingVals{indTrigger,1}),1)]);
    end
    [~, sortIdx] = sort(originalET.event(:,1), 'ascend');
    originalET.event = originalET.event(sortIdx,:);
    
    newET = originalET; clear originalET;
    
    % save updated structure
    save(fileNameOut, '-struct', 'newET');
    
    %Triggers(indFile,1:numel(newET.data(:,9))) = newET.data(:,9);
    
end

% figure;
% plot(Triggers)