#!/bin/bash

# This script prepares tardis by compiling the necessary function in MATLAB.

#ssh tardis # access tardis

# check and choose matlab version
#module avail matlab
module load matlab/R2016b

# compile functions

matlab
%% add fieldtrip toolbox
addpath('/home/mpib/LNDG/StateSwitch/WIP_eeg/SA_preproc_study_YA/T_tools/fieldtrip-20170904/')
ft_defaults()
ft_compile_mex(true)
%% go to analysis directory containing .m-file
cd('/home/mpib/LNDG/StateSwitch/WIP_eeg/SA_preproc_study_YA/A_scripts/H_STSW_automatic_artifact_correction/')
%% compile function and append dependencies
mcc -m H_STSW_automatic_artifact_correction_180123.m -a /home/mpib/LNDG/StateSwitch/WIP_eeg/SA_preproc_study_YA/T_tools/fnct_common -a /home/mpib/LNDG/StateSwitch/WIP_eeg/SA_preproc_study_YA/T_tools/fnct_THG -a /home/mpib/LNDG/StateSwitch/WIP_eeg/SA_preproc_study_YA/T_tools/fnct_MWB/MWB_ArtfDetec