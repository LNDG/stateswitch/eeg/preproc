pn.History = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/A_preproc/SA_preproc_study_YA/B_data/D_History/';

IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';'1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1213';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};
condEEG = 'dynamic';

% % find overwritten entries
% 
% comp = [];
% count = 1;
% for indID = 1:48
%     for indRun = 1:4
%         ID = IDs{indID};
%         load([pn.History, num2str(ID), '_r',num2str(indRun), '_', condEEG, '_config.mat'],'config')
%         configs{count} = config.trl_ica1;
%         if count > 1
%             comp{count}=ismember(configs{count},configs{count-1},'rows');
%             %config.trl_ica1(comp,:) = [];
%             numel(find(comp{count}))
%             %save([pn.History, num2str(ID), '_r',num2str(indRun), '_', condEEG, '_config.mat'],'config')
%         end
%         count = count + 1;
%     end
% end
% 
% % remove overwritten entries

%% alternative: find closest end timepoint

for indID = 1:48
    for indRun = 1:4
        ID = IDs{indID};
        load([pn.History, num2str(ID), '_r',num2str(indRun), '_', condEEG, '_config.mat'],'config')
        endSample = config.nSamples/4;
        [~, comp(indID, indRun)] = min(abs(config.trl_ica1(:,2)-round(endSample)))
        config.trl_ica1 = config.trl_ica1(1:comp(indID, indRun),:);
        save([pn.History, num2str(ID), '_r',num2str(indRun), '_', condEEG, '_config.mat'],'config')
        clear config;
    end
end