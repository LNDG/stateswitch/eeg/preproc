#!/bin/bash

# call the BOSC analysis by session and participant
cd /home/mpib/LNDG/StateSwitch/WIP_eeg/SA_preproc_study/A_scripts/E2_STSW_ica1_OA/

mkdir /home/mpib/LNDG/StateSwitch/WIP_eeg/SA_preproc_study/Y_logs/E2_STSW_ica1_OA

for i in $(seq 45 46); do
	echo "#PBS -N STSW_eeg_ica_OA_${i}" 										> job
	echo "#PBS -l walltime=50:00:00" 										>> job
	echo "#PBS -l mem=8gb" 													>> job
	echo "#PBS -j oe" 														>> job
	echo "#PBS -o /home/mpib/LNDG/StateSwitch/WIP_eeg/SA_preproc_study/Y_logs/E2_STSW_ica1_OA" >> job
	echo "#PBS -m n" 														>> job
	echo "#PBS -d ." 														>> job
	echo "./E2_STSW_ica1_OA_run.sh /opt/matlab/R2016b $i " 			>> job
	qsub job
	rm job
done
done