function E2_STSW_ica1_OA(id)

% 170915 | JQK adapted from MD script
% 180103 | adapted for STSW Study
% 180108 | adapted for tardis

%% initialize

%restoredefaultpath;
%clear all; close all; pack; clc;

%% pathdef
if ismac
%     pn.study        = '/Volumes/LNDG/Projects/StateSwitch/';
%     pn.eeg_root     = [pn.study, 'dynamic/data/eeg/task/A_preproc/SA_preproc_study/'];
    pn.eeg_root =   '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP_eeg/SA_preproc_study/';
    pn.dynamic_In   = [pn.eeg_root, 'B_data/B_EEG_ET_ByRun/'];
    pn.EEG          = [pn.eeg_root, 'B_data/C_EEG_FT/']; mkdir(pn.EEG);
    pn.History      = [pn.eeg_root, 'B_data/D_History/']; mkdir(pn.History);
    % add ConMemEEG tools
    pn.MWBtools     = [pn.eeg_root, 'T_tools/fnct_MWB/'];            addpath(genpath(pn.MWBtools));
    pn.THGtools     = [pn.eeg_root, 'T_tools/fnct_THG/'];            addpath(genpath(pn.THGtools));
    pn.commontools  = [pn.eeg_root, 'T_tools/fnct_common/'];         addpath(genpath(pn.commontools));
    pn.fnct_JQK     = [pn.eeg_root, 'T_tools/fnct_JQK/'];           addpath(genpath(pn.fnct_JQK));
    pn.FT           = [pn.eeg_root, 'T_tools/fieldtrip-20170904/']; addpath(pn.FT); ft_defaults;
else
    pn.root         = '/home/mpib/LNDG/StateSwitch/WIP_eeg/SA_preproc_study/';
    pn.EEG          = [pn.root, 'B_data/C_EEG_FT/']; mkdir(pn.EEG);
    pn.History      = [pn.root, 'B_data/D_History/']; mkdir(pn.History);
    pn.THGtools     = [pn.root, 'T_tools/fnct_THG/'];
    pn.commontools  = [pn.root, 'T_tools/fnct_common/'];
    % add external tools (need to be compiled in)
    %pn.commontools  = [pn.root, 'T_Tools/fnct_common'];         addpath(genpath(pn.commontools));
    %pn.FT           = [pn.root, 'T_Tools/fieldtrip-20170904/']; addpath(pn.FT); ft_defaults;
end

%% define Condition & IDs for preprocessing

condEEG = 'dynamic';

%% define IDs for visual screening

% N = 48;
%IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';'1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1213';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

% N = 53 OAs;
IDs = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

id = str2num(id);

%% loop IDs
%for id = 1:length(IDs)
	display(['processing ID ' num2str(IDs{id})]);
    try
        if 1%~exist([pn.EEG, IDs{id}, '_r',num2str(indRun), '_', condEEG, '_EEG_Rlm_Fhl_Ica.mat'],'file')
            
            for iRun = 1:4
                %%  load raw data & exclude parts containing artifacts

                % load config
                config = [];
                load([pn.History, IDs{id}, '_r',num2str(iRun), '_', condEEG, '_config.mat'],'config');

                config_tmp.(['trl_', num2str(iRun)]) = config.trl_ica1;
                config_tmp.(['visual_inspection_', num2str(iRun)]) = config.visual_inspection;

                % define segment(s) to be read by fieldtrip
                cfg.trl = config.trl_ica1;

                % load data
                dataByRun{iRun} = load([pn.EEG, IDs{id}, '_r',num2str(iRun), '_', condEEG, '_EEG_Raw_Rlm_Flh_Res.mat'],'data_EEG');

                % adjust final length
                cfg.trl(end,2) = size(dataByRun{iRun}.data_EEG.trial{1},2);
                
                % "segment" data
                dataByRun{iRun}.data = cm_segmentation_of_continuous_data_fieldtrip_20150825(dataByRun{iRun}.data_EEG,cfg);    

                % clear cfg structure
                clear cfg

                %%  segmentation for ICA

                % define settings
                cfg.length = 2;
                cfg.n      = 5000;      % keep all possible trials
                cfg.type   = 'rnd';     % select trials randomly if > 1500 trials available
                cfg.seed   = 20170915 + str2num(IDs{id});

                % arbitrary segmentation - segments a 2 sec
                % NOTE original segments will be overwritten
                dataByRun{iRun}.data = cm_arbitrary_segmentation_fieldtrip_20150210(dataByRun{iRun}.data,cfg);
                % data = ft_checkdata(data,'feedback','yes');
            end

            % combine data into one structure for ICA
            data = ft_appenddata(cfg, dataByRun{1}.data, dataByRun{2}.data, dataByRun{3}.data, dataByRun{4}.data);
            clear dataByRun; % JQK: 180219;
            
            %% for select IDs, interpolate channels prior to ICA
                     
            if strcmp(IDs{id}, '2112')
                cfg = [];
                cfg.channel = {'all','-ECG','-A2'};
                dataSelect = ft_preprocessing(cfg,data);
                clear cfg;
                % fieldtrip format electrode information
                load([pn.THGtools, 'electrodelayouts/realistic_1005.mat'])
                data.elec = cm_elec2dataelec_20170919(realistic_1005,dataSelect);
                cfg = [];
                cfg.method     = 'spline';
                cfg.badchannel = {'Fz'; 'FCz'};
                cfg.trials     = 'all';
                cfg.lambda     = 1e-5; 
                cfg.order      = 4; 
                cfg.elec       = data.elec;
                data = ft_channelrepair(cfg,data);
                clear cfg;
            end
            
            %%  ICA

            % date
            dt = date;

            % ica config
            cfg.method           = 'runica';
            cfg.channel          = {'all','-ECG','-A2'};                % additional channel should be excluded already...
            cfg.trials           = 'all';
            cfg.numcomponent     = 'all';
            cfg.demean           = 'no';
            cfg.runica.extended  = 1;
            cfg.runica.logfile   = [pn.History 'log_' IDs{id} '_' condEEG '_ICA1_' dt '.txt'];

            % run ICA
            icadat = ft_componentanalysis(cfg,data);

            %%  automatic ICA labeling

            [iclabels] = cm_automatic_IC_detection_20170919(data,icadat);

            %%  save data for ICA labeling

            config.trl_1 = config_tmp.trl_1;
            config.trl_2 = config_tmp.trl_2;
            config.trl_3 = config_tmp.trl_3;
            config.trl_4 = config_tmp.trl_4;

            config.visual_inspection_1 = config_tmp.visual_inspection_1;
            config.visual_inspection_2 = config_tmp.visual_inspection_2;
            config.visual_inspection_3 = config_tmp.visual_inspection_3;
            config.visual_inspection_4 = config_tmp.visual_inspection_4;

            % - include ICA solution in data
            data.topo 	   = icadat.topo;
            data.unmixing  = icadat.unmixing;
            data.topolabel = icadat.topolabel;
            data.cfg       = icadat.cfg;

            % - include ICA solution in config
            config.ica1.date      = dt;
            config.ica1.topo 	  = icadat.topo;
            config.ica1.unmixing  = icadat.unmixing;
            config.ica1.topolabel = icadat.topolabel;
            config.ica1.cfg       = icadat.cfg;
            config.ica1.iclabels.auto = iclabels;

            % fieldtrip format electrode information
            load([pn.THGtools, 'electrodelayouts/realistic_1005.mat'])
            data.elec = cm_elec2dataelec_20170919(realistic_1005,data);

            % EEGLAB format electrode information
            load([pn.THGtools 'electrodelayouts/chanlocs_eeglab_MPIB_64_electrodes.mat'])
            data = cm_chanlocs2MPIB64_20140126(data,chanlocs);

            % - include channel information in config
            config.elec     = data.elec;
            config.chanlocs = data.chanlocs;

            % keep ICA labels
            data.iclabels = iclabels;

            % save data
            save([pn.EEG, IDs{id}, '_', condEEG, '_EEG_Rlm_Fhl_Ica.mat'],'data')

            % save config
            config.preproc_version = '20170915';
            save([pn.History, IDs{id}, '_', condEEG, '_config.mat'],'config')

            % clear variables
            clear chanlocs cfg config data dt icadat iclabels realistic_1005

         end % file available
    catch ME
        warning('Error occured. Please check.');
        rethrow(ME)
    end % try..catch
%end; clear id

