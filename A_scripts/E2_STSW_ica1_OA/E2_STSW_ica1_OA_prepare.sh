#!/bin/bash

# This script prepares tardis by compiling the necessary function in MATLAB.

#ssh tardis # access tardis

# check and choose matlab version
#module avail matlab
module load matlab/R2016b

# compile functions

matlab
%% add fieldtrip toolbox
addpath('/home/mpib/LNDG/StateSwitch/WIP_eeg/SA_preproc_study/T_tools/fieldtrip-20170904/')
ft_defaults()
ft_compile_mex(true)
%% go to analysis directory containing .m-file
cd('/home/mpib/LNDG/StateSwitch/WIP_eeg/SA_preproc_study/A_scripts/E2_STSW_ica1_OA/')
%% compile function and append dependencies
mcc -m E2_STSW_ica1_OA.m -a /home/mpib/LNDG/StateSwitch/WIP_eeg/SA_preproc_study/T_tools/fnct_common -a /home/mpib/LNDG/StateSwitch/WIP_eeg/SA_preproc_study/T_tools/eeglab14_1_1b/functions/sigprocfunc
exit