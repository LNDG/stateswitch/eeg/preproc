%% Import EyeEEG Data

% 170913 | JQK adapted function from MD's scripts
% 171221 | JQK adapted for study STSWD task
% 180103 | JQK had to change triggers to range from 1 to 64 vs 2 to 128 due
%           to recreated triggers in ET data
%        | offset marker set to 128
%        | exclude subject 1228 run 4
% 180205 | adjusted for OAs
% 180206 | for 2160, initial trigger is missing, excluded for now
%        | for 2203, initial trigger is missing, excluded for now
%        | created this function for subjects 2160 & 2203, run1

% This function links EEG & ET data for subject 2160 & 2203, run1.
% Initial trials that are not available in the EEG data are dropped, a new
% ET matrix containing only overlapping events with the EEG is created and
% the data are linked based on the first shared trial onset.

%% initialize

clear; close all; pack; clc;

%% load in path & toolboxes

pn.study    = '/Volumes/LNDG/Projects/StateSwitch/';
pn.eye_root = [pn.study, 'dynamic/data/eye/SA_EEG/'];
pn.eeg_root = [pn.study, 'dynamic/data/eeg/task/A_preproc/SA_preproc_study/'];
pn.eye_IN   = [pn.eye_root, 'A_preprocessing/B_data/D_eye_mat_edited/'];
pn.eeg_IN   = [pn.eeg_root, 'B_data/A_raw/'];
pn.data_OUT = [pn.eeg_root, 'B_data/B_EEG_ET_ByRun/']; mkdir(pn.data_OUT);
pn.logs     = [pn.eeg_root, 'Y_logs/A_EEG_ET_ByRun_logs/']; mkdir(pn.logs);
pn.eeglab   = [pn.eeg_root, 'T_Tools/eeglab14_1_1b/']; addpath(pn.eeglab);
pn.fnct_JQK = [pn.eeg_root, 'T_Tools/fnct_JQK/']; addpath(genpath(pn.fnct_JQK));
pn.plotFolder = [pn.eeg_root, 'C_figures/A_EEG_ET_ByRun_Figures/']; mkdir(pn.plotFolder);

%% open eeglab

eeglab % use eegh to get history of EEGlab commands!

%% datadef

condEEG = 'dynamic'; % Note: ET data were only recorded during the task.

%% merge EEG & ET data

IDs = {'2160'; '2203'};

for indID = 1:numel(IDs)

    ID = IDs{indID};
    
    % create log
    diary([pn.logs, num2str(ID) ,'_', condEEG , '_ET_EEG_notes_',date,'_run1.txt']);

    %% load raw EEG data by run

    eegIn = ['ss_',num2str(ID), '_dynamic.vhdr'];
    mrkIn = ['ss_',num2str(ID), '_dynamic.vmrk'];
    % load markers
    mrk = bva_readmarker([pn.eeg_IN, mrkIn]);
    % if multiple marker files are available: concatenate them
    if iscell(mrk)
        mrk = cat(2,mrk{:});
    end
    % get number of trial onsets
    trlOnsets = find(mrk(1,:)==20);
    % find run on/offset
    onsets = find(mrk(1,:)== 1); % first onset marker
    offsets = find(mrk(1,:)== 128); % final offset marker [set to 128]
    onsets = [1 onsets];
    % check whether any on/offset markers are incorrect (e.g. restarted presentation)
    excludeOnsets = find(diff(onsets)<1000);
    excludeOffsets = find(diff(offsets)<1000);
    if ~isempty(excludeOnsets)
        onsets(excludeOnsets) = [];
    end
    if ~isempty(excludeOffsets)
        offsets(excludeOffsets) = [];
    end
    tmp_onsetMrk = mrk(:,onsets);
    if strcmp(IDs{indID}, '1228') % subject 1228 had only 3 runs recorded
        tmp_onsetMrk(:,4) = [];
    end
    tmp_offsetMrk = mrk(:,offsets);
    SegmentMat = [tmp_onsetMrk(2,:)', tmp_offsetMrk(2,:)'];
    fprintf(['Run length is: ', num2str(diff(SegmentMat, [], 2)'), '\n']);
    % load by run
    run = 1;
    EEG = pop_loadbv(pn.eeg_IN, eegIn, SegmentMat(run,:));
    [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 0,'setname','data_EEG','gui','off');
    EEGByRun{run} = EEG;
    clear EEG;

    %% merge ET & EEG data by run

    % set paths
    eyeIn = [pn.eye_IN, 'S', num2str(ID), 'r', num2str(run), '.mat'];
    eyeCor = [pn.eye_IN, 'S', num2str(ID), 'r', num2str(run), '_cor.mat'];
    dataOut_eegET = [pn.data_OUT, num2str(ID), '_r', num2str(run), '_', condEEG, '_eyeEEG'];
    dataOut_mrk = [pn.data_OUT, num2str(ID), '_r', num2str(run), '_', condEEG, '_mrk_eyeEEG.mat'];
    fprintf(['... Merging ET & EEG \n']);
    % set beginning/offset markers
    triggers = [20 64]; % second onset trigger, second-to-last offset trigger
    % merge data
    EEG = EEGByRun{run};
    EEG = eeg_checkset(EEG);
    if exist(eyeIn) % only add ET data if it exists
        %% remove periods from the ET data where no overlapping stimuli exist in the EEG recording
        numofStim = numel(find(strcmp({EEG.event.type},'S 20')));
        EyeLoaded = load(eyeIn); % laod ET data
        StimOnsetEventsET = find(EyeLoaded.event(:,2)==20); % find all stimulus onsets in the ET data
        StimOnsetEventsET_lockedwithEEG = StimOnsetEventsET(end-numofStim+1:end); % find those stimulus periods that are also in the EEG recording
        EyeLoaded.event = EyeLoaded.event(StimOnsetEventsET_lockedwithEEG(1):end,:);  % remove events that are not available in the EEG
        save(eyeCor, '-struct', 'EyeLoaded');
        EEG = pop_importeyetracker(EEG,eyeCor,triggers ,[1:9] ,{'TIME' 'L_GAZE_X' 'L_GAZE_Y' 'L_AREA' 'L_VEL_X' 'L_VEL_Y' 'RES_X' 'RES_Y' 'INPUT'},1,1,0,0);
    end
    [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 2,'setname','data_EYE_EEG','savenew',dataOut_eegET,'gui','off');
    % save mrk/event files
    event = EEG.event;
    urevent = EEG.urevent;
    save(dataOut_mrk, 'event', 'urevent')

    diary off
    
end;