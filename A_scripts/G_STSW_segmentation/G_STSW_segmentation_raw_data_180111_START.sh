#!/bin/bash

# call the BOSC analysis by session and participant
cd /home/mpib/LNDG/StateSwitch/WIP_eeg/SA_preproc_study_YA/A_scripts/G_STSW_segmentation/

IDs=(1117 1118 1120 1124 1125 1126 1131 1132 1135 1136 1151 1160 1164 1167 1169 1172 1173 1178 1182 1214 1215 1216 1219 1223 1227 1228 1233 1234 1237 1239 1240 1243 1245 1247 1250 1252 1257 1261 1265 1266 1268 1270 1276 1281 2142 2253 2254 2255)

mkdir /home/mpib/LNDG/StateSwitch/WIP_eeg/SA_preproc_study_YA/Y_logs/G_STSW_segment

for i in $(seq 1 ${#IDs[@]}); do
	echo "#PBS -N STSW_eeg_segment_${i}" 									> job
	echo "#PBS -l walltime=03:00:00" 										>> job
	echo "#PBS -l mem=4gb" 													>> job
	echo "#PBS -j oe" 														>> job
	echo "#PBS -o /home/mpib/LNDG/StateSwitch/WIP_eeg/SA_preproc_study_YA/Y_logs/G_STSW_segment" >> job
	echo "#PBS -m n" 														>> job
	echo "#PBS -d ." 														>> job
	echo "./G_STSW_segmentation_raw_data_180111_run.sh /opt/matlab/R2016b $i " 	>> job
	qsub job
	rm job
done
done