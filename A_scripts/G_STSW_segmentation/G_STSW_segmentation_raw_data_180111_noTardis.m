function G_STSW_segmentation_raw_data_180111_noTardis()

%% G_STSW_segmentation_raw_data_180111

% 170921 | JQK adapted from MD script
% 180118 | adapted for STSW study YA, adapted for tardis

% config.data_file still refers to the non-tardis location

% 180124 | skip data without eye info from processing

%% initialize

% restoredefaultpath;
% clear all; close all; pack; clc;

%% pathdef

if ismac
    pn.study        = '/Volumes/LNDG/Projects/StateSwitch/';
    pn.eeg_root     = [pn.study, 'dynamic/data/eeg/task/A_preproc/SA_preproc_study/'];
    pn.dynamic_In   = [pn.eeg_root, 'B_data/B_EEG_ET_ByRun/'];
    pn.triggerTiming= [pn.eeg_root, 'C_figures/D_TriggerTiming/'];
    pn.EEG          = [pn.eeg_root, 'B_data/C_EEG_FT/']; mkdir(pn.EEG);
    pn.History      = [pn.eeg_root, 'B_data/D_History/']; mkdir(pn.History);
    % add ConMemEEG tools
    pn.MWBtools     = [pn.eeg_root, 'T_tools/fnct_MWB/'];           addpath(genpath(pn.MWBtools));
    pn.THGtools     = [pn.eeg_root, 'T_tools/fnct_THG/'];           addpath(genpath(pn.THGtools));
    pn.commontools  = [pn.eeg_root, 'T_tools/fnct_common/'];        addpath(genpath(pn.commontools));
    pn.fnct_JQK     = [pn.eeg_root, 'T_tools/fnct_JQK/'];           addpath(genpath(pn.fnct_JQK));
    pn.FT           = [pn.eeg_root, 'T_tools/fieldtrip-20170904/']; addpath(pn.FT); ft_defaults;
    pn.helper       = [pn.eeg_root, 'A_scripts/helper/'];           addpath(pn.helper);
else
    pn.root         = '/home/mpib/LNDG/StateSwitch/WIP_eeg/SA_preproc_study/';
    pn.EEG          = [pn.root, 'B_data/C_EEG_FT/'];
    pn.History      = [pn.root, 'B_data/D_History/'];
    pn.triggerTiming= [pn.root, 'C_figures/D_TriggerTiming/'];
end

%% define Condition & IDs for preprocessing

condEEG = 'dynamic';

%% define IDs for segmentation

% N = 47 YAs + 53 OAs;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

%%  loop IDs

clc;
for id = 1:length(IDs)
    display(['processing ID ' num2str(IDs{id})]);
    for iRun = 1:4
        %%  load raw data

        if 1%~exist([pn.EEG, IDs{id}, '_r',num2str(iRun), '_', condEEG, '_eyeEEG_Rlm_Fhl_rdSeg.mat'],'file')

        % load config
        load([pn.History, IDs{id}, '_r',num2str(iRun), '_', condEEG, '_config.mat'],'config');

        % copy ICA labeling info
        configWithICA = load([pn.History, IDs{id}, '_', condEEG, '_config.mat'],'config');

        config.ica1 = configWithICA.config.ica1;

        %% ---- generate segmentation ---- %%

        % load marker file
        mrk = config.mrk;

        for i = 1:size(mrk,2)
            mrk_val{i,1} = mrk(1,i).value;
        end

        % for 2160, the onset marker was not recorded: extract at beginning
        if strcmp(IDs{id}, '2160') && iRun ==1
            mrk_val{2,1} = 'S 17';
        end
        
        % for 2203, the onset marker was not recorded: extract at beginning
        if strcmp(IDs{id}, '2203') && iRun ==1
            mrk_val{2,1} = 'S 17';
        end
        
        % generate trial structure
        indOnset = find(strcmp(mrk_val(:,:),'S 17')); % (fix cue onset trigger = 'S 17')
        indOnset = sortrows(indOnset,1);
        indOffset = find(strcmp(mrk_val(:,:),'S 64')); % (ITI trigger = 'S 64')
        indOffset = sortrows(indOffset,1);

        h = figure;
        plot(diff([mrk(indOnset).sample]))
        hold on; plot(diff([mrk(indOffset).sample]))
        pn.plotFolder = pn.triggerTiming;
        figureName = ['A_triggerTiming_',IDs{id}, '_r',num2str(iRun)]; 
        saveas(h, [pn.plotFolder, figureName], 'png');
        close(h);
                
        % Stim trials
        trl = zeros(length(indOnset),3);
        TOI1 = 1500; % segmentation before trigger
        TOI2 = 1500; % segmentation following trigger

        for j = 1:size(trl,1)
            trl(j,1) = mrk(1,indOnset(j)).sample - TOI1; % segmentation from 1500 ms before stim
            trl(j,2) = mrk(1,indOffset(j)).sample + TOI2; % to 1500 ms after trigger
            trl(j,3) = -TOI1;% offset
        end; clear j
        
        % if an onset occurs before recording onset: set to extract at 1
        if numel(find(trl(:,1)<0))>0
            trl(find(trl(:,1)<0),3) = trl(find(trl(:,1)<0),3)-trl(find(trl(:,1)<0),1);
            trl(find(trl(:,1)<0),1) = 1;
        end

        % add trial structure to config
        config.trl = trl;

        % save config
        save([pn.History, IDs{id}, '_r',num2str(iRun), '_', condEEG, '_config.mat'],'config')

        %% ----  load, filter, and re-reference raw data ---- %%

        % define reading & preprocessing parameters
        % read in all data first

        cfg             = [];
        cfg.datafile    = [config.data_file];                
        cfg.trl         = config.trl;

        cfg.channel     = {'all'};
        cfg.implicitref = 'REF'; % recover implicit reference

        data            = ft_preprocessing(cfg);

         %% SWITCH CHANNELS ACCORDING TO ARRANGEMENT!

        if max(strcmp(IDs{id}, {'1126'; '2227'}))==1
            data = SS_switchChannels_GreenYellow(data);       % green and yellow boxes exchanged
            data = SS_switchChannels_Study_noA1(data);        % TP9 and TP10 exchanged manually
        elseif max(strcmp(IDs{id}, {'1216'}))==1
            data = SS_switchChannels_GreenYellow(data);       % green and yellow boxes exchanged
            data = SS_switchChannels_Study(data);             % TP9(A1) and TP10(FCz) wrongly ordered in workspace
        elseif max(strcmp(IDs{id}, {'1118'; '1215'; '1124'}))==1
            data = SS_switchChannels_Study_noA1(data);        % TP9 and TP10 exchanged manually
        else 
            data = SS_switchChannels_Study(data);             % TP9(A1) and TP10(FCz) wrongly ordered in workspace
        end
        data = SS_changeChannels(data);

        %%
        % select eye data
        cfg         =  [];
        cfg.channel =  {'TIME', 'L_GAZE_X', 'L_GAZE_Y', ...
            'L_AREA', 'L_VEL_X', 'L_VEL_Y', 'RES_X', 'RES_Y', 'INPUT'};

        data_eye        = ft_preprocessing(cfg, data);
        %%
        % select EEG (non-eye/ECG/trigger) data
        cfg                  =  [];
        cfg.channel          = {'all', ...
            '-TIME', '-L_GAZE_X', '-L_GAZE_Y', ...
            '-L_AREA', '-L_VEL_X', '-L_VEL_Y', '-RES_X', '-RES_Y', '-INPUT', '-ECG'};

        data_EEG             = ft_preprocessing(cfg, data);
        %%
        % filter & reref EEG data
        cfg             = [];

        cfg.continuous  = 'yes';
        cfg.demean      = 'yes';

        cfg.reref       = 'yes';
        cfg.refchannel  = {'A1','A2'};
        cfg.implicitref = 'A2';

        cfg.hpfilter    = 'yes';
        cfg.hpfreq      = .2;
        cfg.hpfiltord   = 4;
        cfg.hpfilttype  = 'but';

        cfg.lpfilter    = 'yes';
        cfg.lpfreq      = 125;
        cfg.lpfiltord   = 4;
        cfg.lpfilttype  = 'but';

        % get data
        data_EEG = ft_preprocessing(cfg, data_EEG);
        flt = cfg;

        % clear cfg structure
        clear cfg

        %% ----  resampling [500 Hz] ---- %%

        % define settings for resampling
        cfg.resamplefs = 500;                           % raw_eeg (ICA loop) at srate of 250Hz! now resample raw files (original files) to 500 Hz!
        cfg.detrend    = 'no';
        cfg.feedback   = 'no';
        cfg.trials     = 'all';

        % resample ALL data
        data        = ft_resampledata(cfg,data);
        if str2num(IDs{id})==1223 & (iRun == 2 | iRun == 3 | iRun == 4)
            disp('Eye data skipped');
        elseif str2num(IDs{id})==1228 & (iRun == 4)
            disp('Eye data skipped');
        else
            data_eye = ft_resampledata(cfg,data_eye);
        end
        data_EEG = ft_resampledata(cfg,data_EEG);

        resample = cfg;
        % clear variables
        clear cfg

        % update config
        config.seg.flt = flt;
        config.seg.resample = resample;

        % save config, data
        save([pn.EEG, IDs{id}, '_r',num2str(iRun), '_', condEEG, '_eyeEEG_Rlm_Fhl_rdSeg'],'data')
        if str2num(IDs{id})==1223 & (iRun == 2 | iRun == 3 | iRun == 4)
            disp('Eye data skipped');
        elseif str2num(IDs{id})==1228 & (iRun == 4)
            disp('Eye data skipped');
        else
            save([pn.EEG, IDs{id}, '_r',num2str(iRun), '_', condEEG, '_eye_Rlm_Fhl_rdSeg'],'data_eye')
        end
        save([pn.EEG, IDs{id}, '_r',num2str(iRun), '_', condEEG, '_EEG_Rlm_Fhl_rdSeg'],'data_EEG')
        save([pn.History, IDs{id}, '_r',num2str(iRun), '_', condEEG, '_config'],'config')

        % clear variables
        clear config data data_eye data_EEG indOnset indOffset mrk mrk_val trl TOI1 TOI2

        else
            disp(['Data already exists: ', IDs{id}, ' Run ',num2str(iRun)]);
        end % exist
    end; clear iRun
end; clear id
