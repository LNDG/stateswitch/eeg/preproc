function EEG = SS_changeChannels(EEG)

    % 180111 | JQK created function
    
    % INPUT: EEG | fieldtrip EEG preprocessing structure
    
    % Switches channels according to LNDG layout.

    LNDGchans = {'Fp1' 'Fp2' 'AF7' 'AF3' 'AF4' 'AF8' 'F7' 'F5' 'F3' 'F1' 'Fz' 'F2' 'F4' 'F6' 'F8' 'FT7' 'FC5' 'FC3' 'FC1' 'FCz' 'FC2' 'FC4' 'FC6' 'FT8' 'T7' 'C5' 'C3' 'C1' 'Cz' 'C2' 'C4' 'C6' 'T8' 'TP7' 'CP5' 'CP3' 'CP1' 'CPz' 'CP2' 'CP4' 'CP6' 'TP8' 'P7' 'P5' 'P3' 'Pz' 'P2' 'P4' 'P6' 'P8' 'PO9' 'PO7' 'PO3' 'POz' 'PO4' 'PO8' 'PO10' 'O1' 'Oz' 'O2' 'A1' 'IOR' 'LHEOG' 'RHEOG' 'ECG'};

    A = LNDGchans;
    B = {EEG.chanlocs(1:65).labels};

    [n,m]=size(A);
    [~,idx]=ismember(A(:),B(:));
    [ii,jj]=ind2sub([n m],idx);

    jj = [jj; [66:numel(EEG.chanlocs)]'];
    
    EEG.data = EEG.data(jj,:);
    %EEG.chanlocs.labels = [B(jj(1:65)), {EEG.chanlocs(66:end).labels}];
    
end