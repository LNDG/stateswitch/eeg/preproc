function EEG = SS_switchChannels_GreenYellow(EEG)

    % 180111 | JQK created function
    
    % INPUT: EEG | fieldtrip EEG preprocessing structure
    
    % Uses study electrode setup. This is to be used in the case where
    % likely the green and yellow channels have been mixed up as observed
    % from the channel*channel correlations.

    green = {'Fp1';'Fp2';'F7';'F3';'Fz';'F4';'F8';'FC5';'FC1';'FC2';'FC6';'T7';'C3';'Cz';'C4';'T8';'A1';'CP5';'CP1';'CP2';'CP6';'FCz';'P7';'P3';'Pz';'P4';'P8';'PO9';'O1';'Oz';'O2';'PO10'};
    yellow = {'AF7';'AF3';'AF4';'AF8';'F5';'F1';'F2';'F6';'HEOGL';'FT7';'FC3';'FC4';'FT8';'HEOGR';'C5';'C1';'C2';'C6';'TP7';'CP3';'CPz';'CP4';'TP8';'P5';'VEOG';'P2';'P6';'PO7';'PO3';'POz';'PO4';'PO8'};
    
    % change all channels to what they should have been recorded as (setup label)
    for indChan = 1:size(green,1)
        channel1 = green(indChan,1);
        channel2 = yellow(indChan,1);
        idx1 = find(strcmp({EEG.chanlocs(:).labels},channel1));
        idx2 = find(strcmp({EEG.chanlocs(:).labels},channel2));
        EEG.chanlocs(idx1).labels = channel2{1};
        EEG.chanlocs(idx2).labels = channel1{1};
    end
    
end