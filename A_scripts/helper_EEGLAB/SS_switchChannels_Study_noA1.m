function EEG = SS_switchChannels_Study_noA1(EEG)

    % 170915 | JQK created function
    % 180109 | JQK altered for STSW study
    %        | channels should have been recorded correctly, only renaming to fit ConMem scheme
    
    % INPUT: EEG | eeglab EEG preprocessing structure
    
    % Use for EEG, where A1 and FCz have NOT been exchanged.

    alteredChans = {'HEOGL' 'LHEOG';'HEOGR' 'RHEOG';'VEOG' 'IOR'};

    % change special channels to what we want them to be
    for indChan = 1:size(alteredChans,1)
        channelRecorded = alteredChans{indChan,1};
        channelIntended = alteredChans(indChan,2);
        EEG.chanlocs(strcmp({EEG.chanlocs(:).labels},channelRecorded)).labels = channelIntended;
    end
    
end