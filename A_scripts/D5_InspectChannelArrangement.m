pn.study        = '/Volumes/LNDG/Projects/StateSwitch/';
pn.eeg_root     = [pn.study, 'dynamic/data/eeg/task/A_preproc/SA_preproc_study_YA/'];
pn.EEG          = [pn.eeg_root, 'B_data/C_EEG_FT/'];
pn.History      = [pn.eeg_root, 'B_data/D_History/'];
pn.scripts      = [pn.eeg_root, 'A_scripts']; addpath(pn.scripts);
pn.figures      = [pn.eeg_root, 'C_figures/C_ChannelCheck/']; mkdir(pn.figures);
% add ConMemEEG tools
pn.MWBtools     = [pn.eeg_root, 'T_tools/fnct_MWB/'];            addpath(genpath(pn.MWBtools));
pn.THGtools     = [pn.eeg_root, 'T_tools/fnct_THG/'];            addpath(genpath(pn.THGtools));
pn.commontools  = [pn.eeg_root, 'T_tools/fnct_common/'];         addpath(genpath(pn.commontools));
pn.fnct_JQK     = [pn.eeg_root, 'T_tools/fnct_JQK/'];           addpath(genpath(pn.fnct_JQK));
pn.FT           = [pn.eeg_root, 'T_tools/fieldtrip-20170904/']; addpath(pn.FT); ft_defaults;

%% define IDs for visual screening
% without 1213
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';'1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

for id = 40:numel(IDs)
    disp(num2str(id))
    data_EEG = [];
    load([pn.EEG, num2str(IDs{id}), '_r1_dynamic_EEG_Raw_Rlm_Flh_Res.mat'],'data_EEG')
    tmp = corrcoef(data_EEG.trial{1}');
    Rmat(id,1:66,1:66) = tmp(1:66,1:66);
end

h = figure;
for indID = 1:size(Rmat,1)
    subplot(6,8,indID);
    imagesc(squeeze(Rmat(indID,:,:)));
    title(IDs{indID});
end
pn.plotFolder = pn.figures;
figureName = 'ChannelCheck_correlation';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');
