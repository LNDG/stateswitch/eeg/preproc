% Check how many ET datasets have the appropriate triggers.

% 180103 | written by JQK

folderNames = dir('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/SA_EEG_YA/A_preprocessing/B_data/C_eye_mat/');
folderNames = {folderNames(:).name};
folderNames = folderNames(3:end);

for indFile = 1:numel(folderNames)
    
    fileName = ['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/SA_EEG_YA/A_preprocessing/B_data/C_eye_mat/', folderNames{indFile}];
    load(fileName,'event')
    
    dataInfoName{indFile,1} = folderNames{indFile};
    dataInfo(indFile,1) = size(event,1);
    
end


%% find out the average lag of the 128 trigger (pilot data)

folderNames = dir('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/PA_EEG_YA/A_preprocessing/B_data/C_eye_mat/');
folderNames = {folderNames(:).name};
folderNames = folderNames(4:end);

for indFile = 1:numel(folderNames)
    
    fileName = ['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/PA_EEG_YA/A_preprocessing/B_data/C_eye_mat/', folderNames{indFile}];
    load(fileName)
    
    idx = find(data(:,9)==128);
    TriggDelay(indFile,1) = idx(1);
    idx = find(data(:,9)==64);
    TriggDelay(indFile,2) = idx(end-5);
    
end