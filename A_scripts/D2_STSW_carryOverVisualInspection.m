% Carry over the manual visual segmentation to new structure.
% This is only necessary when C_prepare_preprocessing has been re-run after visual inspection.

% N = 47 (1213 excluded);
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';'1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

%% loop IDs

% rename history folder to '_new'; unzip archived history folder

pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/A_preproc/SA_preproc_study_YA/';
pn.History_old = [pn.root, 'B_data/D_History_180109/'];
pn.History_new = [pn.root, 'B_data/D_History/'];

for id = 1:length(IDs)
    disp(['Subject ', num2str(id)]);
    for indRun = 1:4
        config_old = load([pn.History_old,IDs{id},'_r',num2str(indRun),'_dynamic_config.mat']);
        config_new = load([pn.History_new,IDs{id},'_r',num2str(indRun),'_dynamic_config.mat']);
        config_new.config.visual_inspection = config_old.config.visual_inspection;
        config_new.config.trl_ica1 = config_old.config.trl_ica1;
        config = config_new.config;
        save([pn.History_new,IDs{id},'_r',num2str(indRun),'_dynamic_config.mat'], 'config');
    end
end

% delete previously unzipped 'Z_History' folder, remove '_new'
