%% check stimulus jitter (should have a 30 Hz update rate)

pn.study        = '/Volumes/LNDG/Projects/StateSwitch/';
pn.eeg_root     = [pn.study, 'dynamic/data/eeg/task/A_preproc/SA_preproc_study/'];
pn.History      = [pn.eeg_root, 'B_data/D_History/'];
pn.dataOut      = [pn.eeg_root, 'B_data/E_various/'];

% N = 47 YAs + 53 OAs;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

%% extract event onsets

%trig.eventDurInS = [1, 2, 3, 2];
trig.eventTriggers = {'S 20'; 'S 96'; 'S 24'};
trig.events = {'Stim Onset'; 'Stim Update'; 'Probe Onset'};

for indID = 1:length(IDs)
    for indRun = 1:4
        try
            disp(['Loading ID ',IDs{indID}, ' Run ', num2str(indRun)]);
            % load config
            load([pn.History, IDs{indID}, '_r',num2str(indRun), '_dynamic_config.mat'],'config');
             % load marker file
            mrk_val = []; mrk = [];
            mrk = config.mrk;
            for i = 1:size(mrk,2)
                mrk_val{i,1} = mrk(1,i).value;
            end
            events{indID,indRun} = NaN(64,50);
            indOnset = find(strcmp(mrk_val(:,:),trig.eventTriggers{1}));
            indOffset = find(strcmp(mrk_val(:,:),trig.eventTriggers{3}));
            for indTrial = 1:numel(indOnset)
               indStims = find(strcmp(mrk_val(indOnset(indTrial):indOffset(indTrial),1),...
                   trig.eventTriggers{2}));
               targetSamples = [mrk(indOnset(indTrial)+indStims-1).sample];
               events{indID,indRun}(indTrial, 1:numel(targetSamples)) = targetSamples;
            end
            % calculate temporal difference
            eventsTempDiff{indID,indRun} = [zeros(size(events{indID,indRun},1),1), diff(events{indID,indRun},[],2)];
        catch
            warning(['Problem with ID ',IDs{indID}, ' Run ', num2str(indRun)]);
            continue;
        end
    end
end

save([pn.dataOut, 'Z_stimulusJitter.mat'], 'events', 'eventsTempDiff')

%% check event jitter across subjects

eventsCat = [];
for indID = 1:numel(IDs)
    for indRun = 1:4
        if ~isempty(events{indID,indRun}) %&& size(events{indID,indRun},2) == 5
            eventsCat = [eventsCat, reshape(events{indID,indRun}', 1, [])];
        end
    end
end
eventsCat(isnan(eventsCat)) = [];

eventsTempDiffCat = [];
for indID = 1:numel(IDs)
    for indRun = 1:4
        if ~isempty(eventsTempDiff{indID,indRun}) %&& size(events{indID,indRun},2) == 5
            eventsTempDiffCat = [eventsTempDiffCat, reshape(eventsTempDiff{indID,indRun}', 1, [])];
        end
    end
end
eventsTempDiffCat(isnan(eventsTempDiffCat)) = [];
eventsTempDiffCat(eventsTempDiffCat==0) = [];

h = figure('units','normalized','position',[0 0 .7 1]);
subplot(4,1,1); plot(eventsCat); xlim([1, size(eventsCat,2)])
subplot(4,1,2); plot(eventsTempDiffCat); xlim([1, size(eventsTempDiffCat,2)])
xlabel('Trial (concatenated across subjects)'); ylabel('Difference between updates (in ms)');
title('Difference between updates was supposed to be 30 Hz (33 ms difference between updates)')
subplot(4,1,3); plot(eventsTempDiffCat(1:1000)); xlim([1, 1000]);
xlabel('Trial (concatenated across subjects)'); ylabel('Difference between updates (in ms)');
title('YA have encoding of stimulus triggers @ frequency of 16 Hz (1/.06)')
subplot(4,1,4); plot(eventsTempDiffCat(8*10^5:8*10^5+1000)); xlim([1, 1000])
xlabel('Trial (concatenated across subjects)'); ylabel('Difference between updates (in ms)');
title('OA have encoding of stimulus triggers @ frequency of 10 Hz (1/.1)')

suptitle('Trigger jitter concatenated across subjects (first YA, then OA)');
set(findall(gcf,'-property','FontSize'),'FontSize',15)

pn.plotFolder = [pn.eeg_root, 'C_figures/E_TriggerJitter/'];
figureName = 'B_stimJitter_EEGtriggerBased';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');
