%% check event jitter

pn.study        = '/Volumes/LNDG/Projects/StateSwitch/';
pn.eeg_root     = [pn.study, 'dynamic/data/eeg/task/A_preproc/SA_preproc_study/'];
pn.History      = [pn.eeg_root, 'B_data/D_History/'];
pn.dataOut      = [pn.eeg_root, 'B_data/E_various/'];

% N = 47 YAs + 53 OAs;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

%% extract event onsets

trig.eventDurInS = [1, 2, 3, 2];
trig.eventTriggers = {'S 17'; 'S 72'; 'S 20'; 'S 24'; 'S 64'};
trig.events = {'Cue Onset'; 'Fix Onset'; 'Stim Onset'; 'Probe Onset'; 'ITI Onset'};

for indID = 1:length(IDs)
    for indRun = 1:4
        try
            disp(['Loading ID ',IDs{indID}, ' Run ', num2str(indRun)]);
            % load config
            load([pn.History, IDs{indID}, '_r',num2str(indRun), '_dynamic_config.mat'],'config');
             % load marker file
            mrk_val = []; mrk = [];
            mrk = config.mrk;
            for i = 1:size(mrk,2)
                mrk_val{i,1} = mrk(1,i).value;
            end
            % go through triggers to get jitter
            for indTrigger = 1:numel(trig.eventTriggers)
                indOnset = [];
                indOnset = find(strcmp(mrk_val(:,:),trig.eventTriggers{indTrigger}));
                events{indID,indRun}(:, indTrigger) = [mrk(indOnset).sample];
            end
            % calculate temporal difference
            events{indID,indRun} = [zeros(size(events{indID,indRun},1),1), diff(events{indID,indRun},[],2)];
        catch
            warning(['Problem with ID ',IDs{indID}, ' Run ', num2str(indRun)]);
            continue;
        end
    end
end

save([pn.dataOut, 'Z_eventJitter.mat'], 'events')

%% check event jitter across subjects

% why does 2160 only have two initial triggers

eventsCat = [];
for indID = 1:numel(IDs)
    for indRun = 1:4
        if ~isempty(events{indID,indRun}) && size(events{indID,indRun},2) == 5
            eventsCat = [eventsCat; events{indID,indRun}];
        end
    end
end

triggerEvents = {'Duration Cue (1 s)'; 'Duration Fixation (2 s)'; 'Duration Stimulus (3 s)'; 'Duration Probe (2 s)'};
triggerDuration = 1000*[1, 2, 3, 2];

h = figure('units','normalized','position',[0 0 .7 1]);
for indEvent = 1:4
    subplot(4,1,indEvent); plot(eventsCat(:,indEvent+1)-triggerDuration(indEvent));
    xlabel('Trial (concatenated across subjects)'); ylabel('Jitter (in ms)');
    xlim([1 size(eventsCat,1)])
    title(triggerEvents{indEvent})
end
suptitle('Trigger jitter concatenated across subjects (first YA, then OA)');
set(findall(gcf,'-property','FontSize'),'FontSize',15)

pn.plotFolder = [pn.eeg_root, 'C_figures/E_TriggerJitter/'];
figureName = 'A_eventJitter';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');
